require 'roo'

#====== modify these file paths!!!!" ======
IOS_EN = "output.ios-en.strings"
IOS_VI = "output.ios-vi.strings"
ANDROID_EN = "output.android-en.xml"
ANDROID_VI = "output.android-vi.xml"
EXCEL_FILE = "MoolaLocalization.xlsx"
#==========================================

KEY_INDEX = 'A'
IOS_EN_INDEX = 'B'
ANDROID_EN_INDEX = 'C'
IOS_VI_INDEX = 'D'
ANDROID_VI_INDEX = 'E'


def write_ios_file(file, key, value)
	file.write "\"#{key}\" = \"#{value}\";" unless value == nil || value.empty?	
	file.write "\n"
end

def write_android_file(file, key, value)	
	file.write "\t<string name=\"#{key}\">#{value}</string>" unless value == nil || value.empty?		
	file.write "\n"
end


ios_en_f = File.open(IOS_EN, "w")
ios_vi_f = File.open(IOS_VI, "w")

android_en_f = File.open(ANDROID_EN, "w")
android_vi_f = File.open(ANDROID_VI, "w")
android_en_f.write "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<resources>\n"
android_vi_f.write "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<resources>\n"

sheet = Roo::Spreadsheet.open EXCEL_FILE
sheet.default_sheet = sheet.sheets[0]

last = sheet.last_row
(2..last).each do |i|	
    key = sheet.cell(i,KEY_INDEX)
    puts key
    write_ios_file ios_en_f, key, sheet.cell(i,IOS_EN_INDEX)
    write_ios_file ios_vi_f, key, sheet.cell(i,IOS_VI_INDEX)
    write_android_file android_en_f, key, sheet.cell(i,ANDROID_EN_INDEX)
    write_android_file android_vi_f, key, sheet.cell(i,ANDROID_VI_INDEX)
end

ios_en_f.close
ios_vi_f.close

android_en_f.write "</resources>"
android_vi_f.write "</resources>"
android_en_f.close
android_vi_f.close