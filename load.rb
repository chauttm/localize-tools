require 'spreadsheet'

#====== modify these file paths!!!!" ======
IOS_EN = "Localizable-en.strings"
IOS_VI = "Localizable-vi.strings"
ANDROID_EN = "strings-en.xml"
ANDROID_VI = "strings-vi.xml"
EXCEL_FILE = "localize.xls"
#==========================================

def insert(map, key, value, index) 
	if map[key] == nil
		map[key] = [ "", "", "", "" ]
	end
	map[key][index] = value
end

def load_ios(map, file_name, value_index)
	f = File.open(file_name, "r")
	contents = f.read
	lines = contents.gsub(/\/\/.*$/, " ").gsub("\n", "").gsub(/\/\*.*\*\//,"").split(";")
	lines.each do |line|	  	
  		tokens = line.split(/\"\s*=\s*\"/)  	
  		unless tokens[0] == nil || tokens[0].empty?
  			puts "____#{tokens[0]}___#{tokens[1]}____"
  			key = tokens[0].sub(/^\s*\"/, "")
  			value = tokens[1].sub(/\"\s*$/, "")
	  		insert map, key, value, value_index
		end
	end
	f.close
end

def load_android(map, file_name, value_index)
	f = File.open(file_name, "r")
	contents = f.read
	lines = contents.gsub(/\/\/.*$/, " ").gsub(/<\?xml.*$/, " ").gsub(/<!--[^-]*-->/, "").gsub("\n", "").split("<string ")

	lines.each do |line|	
  		tokens = line.split(/[\"><]/)  	
  	
		unless tokens[1]==nil || tokens[3] == nil		
			key = tokens[1]
			value = tokens[3].gsub(/\s\s+/, " ")
			insert map, key, value, value_index
		end	
	end
	f.close
end

map = Hash.new (nil)
IOS_EN_INDEX = 0
ANDROID_EN_INDEX = 1
IOS_VI_INDEX = 2
ANDROID_VI_INDEX = 3

load_ios(map, IOS_EN, IOS_EN_INDEX)
load_ios(map, IOS_VI, IOS_VI_INDEX)
load_android(map, ANDROID_EN, ANDROID_EN_INDEX)
load_android(map, ANDROID_VI, ANDROID_VI_INDEX)

book = Spreadsheet::Workbook.new
sheet = book.create_worksheet
row = 0;
sheet[row,0] = "key"
sheet[row,IOS_EN_INDEX + 1] = "IOS_EN"
sheet[row,IOS_VI_INDEX + 1] = "IOS_VI"
sheet[row,ANDROID_EN_INDEX + 1] = "ANDROID_EN"
sheet[row,ANDROID_VI_INDEX + 1] = "ANDROID_VI"

sheet[0, 5] = "en differ"
sheet[1, 5] = "=(B2<>C2)"
sheet[row, 6] = "vi differ"
sheet[1, 6] = "=(D2<>E2)"

map.sort.each do |key, value| 
	#puts "#{key};#{value[0]};#{value[1]};#{value[2]};#{value[3]}"
	row += 1
	sheet[row,0] = key
	sheet[row,1] = value[0]
	sheet[row,2] = value[1]
	sheet[row,3] = value[2]
	sheet[row,4] = value[3]
end
book.write EXCEL_FILE