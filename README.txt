==============================
     Preparations !!!!!
==============================
1. install Ruby, 2.0.0 is recommended.

2. from the project directory, run
bundle install

=====================================================================================
A. How to generate localization files from the Excel file
=====================================================================================

When you've done editing the excel file in GoogleSpreadsheet, download it in Excel format
to this directory. The default file name should be the same as in the source code.
If it is not, you can either change the file name or modify gen.rb.

Then you can generate iOS/Android localization files 
by the command:

ruby gen.rb

You will see outputs in files out.ios....strings and out.android.....xml

===========================================================================
B. How to generate common Excel file from iOS and Android localization files:
===========================================================================

1. modify load.rb to update paths to files, samples files are given.
#====== modify these file paths!!!!" ======
IOS_EN = "Localizable-en.strings"
IOS_VI = "Localizable-vi.strings"
ANDROID_EN = "strings-en.xml"
ANDROID_VI = "strings-vi.xml"
EXCEL_FILE = "localize.xls"
#==========================================

2. run
ruby load.rb

3. open localize.xls to see the results.
F2 and G2 are formulas to check if iOS's versions are different from Android's versions.
You should touch the formulas and press Enter in order to make them work,
then copy & paste them to the whole columns (select the cell then drag the bottom left corner of the cell down).
After that, you can sort/filter to see the problematic rows better.

